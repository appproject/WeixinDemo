import { init } from "/common/userInfoManager.js"

App({
  //自定义配置
  settings : {
    debug: true, //是否调试模式
  },

  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
    init();
  },
})