import {log, requestScrollerWindowHeight } from '../../utils/util';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsList: [],
    screenWidth: 750,
    scollerHeight: 100,
    selectAll: false,
    totalAmount: 0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    requestScrollerWindowHeight(this.onUpdateWindowHeight, 200)

    wx.request({
      url: 'https://www.easy-mock.com/mock/5ab22581584a4217a30fc936/test/carts',
      success: (res) => {
        log(res.data.data.goods);
        this.setData({
          goodsList: res.data.data.goods
        })
      }
    })
  },

  onUpdateWindowHeight : function(height) {
    this.setData({
      screenWidth: 750,
      scollerHeight: height,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  //编辑购物车
  editCart: function (e) {
    wx.showToast({
      title: 'Edit cart',
    })
  },

  //减去商品数量
  reduceCount: function (e) {
    const index = e.currentTarget.id;
    if (this.data.goodsList[index].count <= 1) {
      this.data.goodsList[index].count = 1;
      wx.showModal({
        title: '数量小于1',
        content: '不允许操作',
        duration: 2000
      })
    } else {
      this.data.goodsList[index].count--;
    }
    this.updateTotalAmount()
    this.setData({
      goodsList: this.data.goodsList,
      totalAmount: this.data.totalAmount,
    })
  },
  //添加商品数量
  addCount: function (e) {
    const index = e.currentTarget.id;
    this.data.goodsList[index].count++;
    this.updateTotalAmount()
    this.setData({
      goodsList: this.data.goodsList,
      totalAmount: this.data.totalAmount,
    })
  },

  //商品选择
  goodsSelectTap: function (e) {
    const index = e.currentTarget.id
    this.data.goodsList[index].select = !this.data.goodsList[index].select
    this.updateTotalAmount()
    this.setData({
      goodsList: this.data.goodsList,
      totalAmount: this.data.totalAmount,
    })
  },

  //选择所有
  allSelectTap: function (e) {
    for (let i = 0; i < this.data.goodsList.length; i++) {
      this.data.goodsList[i].select = !this.data.selectAll
    }
    this.updateTotalAmount()
    this.setData({
      selectAll: !this.data.selectAll,
      goodsList: this.data.goodsList,
      totalAmount: this.data.totalAmount,
    })
  },

  //更新总金额
  updateTotalAmount: function () {
    var total = 0
    const goods = this.data.goodsList;
    for (let i = 0; i < goods.length; i++) {
      if (goods[i].select) {
        total += goods[i].count * goods[i].price
      }
    }
    this.data.totalAmount = total
  },

  //结算
  settleTap: function (e) {

  }
})