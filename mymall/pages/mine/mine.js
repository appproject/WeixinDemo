// pages/mine/mine.js
const userInfoManager = require("../../common/userInfoManager.js")

Page({
  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {
      avatar: "",
      nickName: "N/A"
    },
    menus: [
      { id: 1, image: "../../images/activity.png", name: "我的订单" },
      { id: 2, image: "../../images/add.png", name: "收货地址" },
      { id: 3, image: "../../images/cart1.png", name: "购物车" },
      { id: 4, image: "../../images/ld.png", name: "系统通知" },
      { id: 5, image: "../../images/card.png", name: "会员卡" },
      { id: 6, image: "../../images/yhj.png", name: "优惠券" },
      { id: 7, image: "../../images/pin.png", name: "我的拼团" },
    ]
  },

  menuClick: function (e) {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.onUserInfoUpdate()
    userInfoManager.addUserInfoCallback(this.onUserInfoUpdate)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (!userInfoManager.isLogin()) {
      userInfoManager.requestWXLogin()
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    userInfoManager.removeUserInfoCallback(this.onUserInfoUpdate);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  //定义用户信息更新callback 收到callback之后重新更新UI
  onUserInfoUpdate: function () {
    console.log('mine userinfo update')
    var userinfo = userInfoManager.getUserInfo();
    if (userinfo) {
      this.setData({
        'userInfo.avatar': userinfo.avatarUrl,
        'userInfo.nickName': userinfo.nickName,
      })
    }
  }
})