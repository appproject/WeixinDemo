// pages/groups/groups.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isLoading: false,
    goodsList: [],
  },

  requestData: function (refresh) {
    this.data.isLoading = true;
    let that = this;
    wx.request({
      url: 'https://www.easy-mock.com/mock/5ab22581584a4217a30fc936/test/groupList',
      success: (res) => {
        wx.hideNavigationBarLoading();
        wx.stopPullDownRefresh();
        if (refresh) {
          that.setData({
            goodsList: res.data.data.goods
          })
        } else {
          let newlist = [];
          newlist = that.data.goodsList.concat(res.data.data.goods);
          that.setData({
            goodsList: newlist
          })
        }
        this.data.isLoading = false;
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.requestData(true);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading();
    this.requestData(true);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (!this.data.isLoading) {
      this.requestData(false);
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  itemClicked: function (e) {
    let index = e.currentTarget.id;
      wx.navigateTo({
        url: '/pages/goodsdetail/goodsdetail?id=' + index,
      })
  }
})