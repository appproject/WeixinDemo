import { log, requestScrollerWindowHeight } from '../../utils/util';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',
    goodsInfo: null,
    screenWidth: 750,
    detailContentHeight: 100,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.data.id = options.id;
    let that = this;
    wx.request({
      url: 'https://www.easy-mock.com/mock/5ab22581584a4217a30fc936/test/goodsDetail',
      success: res => {
        that.setData({
          goodsInfo: res.data.data,
        })
      }
    })

    requestScrollerWindowHeight(this.onUpdateWindowHeight, 100)
  },

  //window更新通知
  onUpdateWindowHeight: function (height) {
    this.setData({
      screenWidth: 750,
      detailContentHeight: height,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  //跳转购物车
  gotoCart: function (e) {
    wx.switchTab({
      url: '../../pages/cart/cart',
    })
  },

  //跳转店铺
  gotoShop: function (e) {

  },

  //加入购物车
  addToCart: function (e) {

  },

  //立即购买
  buyNow: function (e) {

  }
})