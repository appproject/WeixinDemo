//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    indicatorDots: true,
    autoPlay: true,
    interval: 3000,
    duration: 800,
    imgUrls: ["http://img.weiye.me/zcimgdir/album/file_59c081abe9cff.png",
      "http://img.weiye.me/zcimgdir/album/file_59b102f038b65.png"],
    currentBannerIndex: 0,

    desLabels: [
      { id: 1, text: "正品保障" },
      { id: 2, text: "极速发货" },
      { id: 3, text: "七天退换" },
    ],

    recommends : [
      { id: 1, title: "流行趋势", des: "2018年最值得购买", src: "../../images/popular.jpg"},
      { id: 2, title: "美妆大牌", des: "时尚博主美妆品牌推荐", src: "../../images/beautiful.jpg"},
      { id: 3, title: "好物穿搭", des: "完美搭配任你挑选", src: "../../images/good.jpg"},
      { id: 4, title: "新品预告", des: "新品上线期待", src: "../../images/new.jpg"},
    ],

    sections:[
      { id: 1, src: "http://img.weiye.me/zcimgdir/album/file_59ba486b8250a.png" },
      { id: 1, src: "http://img.weiye.me/zcimgdir/album/file_59ba490ec1b2f.png" },
      { id: 1, src: "http://img.weiye.me/zcimgdir/album/file_59ba4949722a8.png" },
      { id: 1, src: "http://img.weiye.me/zcimgdir/album/file_59b663e135edd.png" },
    ],
    
    notices : [
      "这是第一条测试消息",
      "这是第二条测试消息",
      "这是第三条测试消息"
    ],

    goods: [],
  },

  bannerChange : function (e){
    this.setData({
      currentBannerIndex : e.detail.current
    })
  },

  bannerTap : function(e){
    wx.showToast({
      title: this.data.currentBannerIndex + ' clicked',
    })
  },

  desLabelTap: function(e) {
    var id = e.currentTarget.id;
    console.log("selectionId " + id)
    if( id == 1){
      wx.navigateTo({
        url: '../test/test'
      })
    } else if(id == 2){
      wx.navigateTo({
        url: '../logs/logs'
      })
    } else if(id == 3){
      wx.showToast({
        title: 'selectionId'  + id,
      })
    }
  },

  recommendClick:function(e){
    
  },

  onPullDownRefresh:function(){
    console.log('--------下拉刷新-------')
    wx.showNavigationBarLoading() //在标题栏中显示加载
    var that = this;
    setTimeout(function () {
      wx.hideNavigationBarLoading();    
      wx.stopPullDownRefresh();  
      that.setData({
        notices: [
          "刷新后第一条测试消息",
          "刷新后第二条测试消息",
          "刷新后第三条测试消息"
        ],
      });
    }, 3000);
  },

  onLoad : function(){
    wx.request({
      url: 'https://www.easy-mock.com/mock/5a27c7a27bf3ee170dc24b18/buygoods/buygoods',
      success : (res) => {
        console.log(res.data.data.goods);
        this.setData({
          goods: res.data.data.goods
        })
      }
    })
  }
})
