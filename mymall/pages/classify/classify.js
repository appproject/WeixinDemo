// pages/classify/classify.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
      list : [
        { id: 1, img: "../../images/c1.png"},
        { id: 1, img: "../../images/c2.png" },
        { id: 1, img: "../../images/c3.png" },
        { id: 1, img: "../../images/c4.png"},
        { id: 1, img: "../../images/c5.png" },
        { id: 1, img: "../../images/c6.png" },
        { id: 1, img: "../../images/c1.png" },
        { id: 1, img: "../../images/c2.png" },
        { id: 1, img: "../../images/c3.png" },
        { id: 1, img: "../../images/c4.png" },
      ],
      loadingMore : true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    let that = this;
     wx.showNavigationBarLoading();
     setTimeout(function(){
       wx.hideNavigationBarLoading();
       wx.stopPullDownRefresh();
       that.setData({
         list: [
           { id: 1, img: "../../images/c1.png" },
           { id: 1, img: "../../images/c2.png" },
           { id: 1, img: "../../images/c3.png" },
           { id: 1, img: "../../images/c4.png" },
           { id: 1, img: "../../images/c5.png" },
           { id: 1, img: "../../images/c6.png" },
           { id: 1, img: "../../images/c1.png" },
           { id: 1, img: "../../images/c2.png" },
           { id: 1, img: "../../images/c3.png" },
           { id: 1, img: "../../images/c4.png" },
         ],
       });
     }, 3000);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log("onReachBottom");
    let that = this;
     wx.showNavigationBarLoading();
     wx.stopPullDownRefresh();
     setTimeout(function(){
       let newlist = [];
       let dataList = [
         { id: 1, img: "../../images/c1.png" },
         { id: 1, img: "../../images/c2.png" },
         { id: 1, img: "../../images/c3.png" },
         { id: 1, img: "../../images/c4.png" },
         { id: 1, img: "../../images/c5.png" },
         { id: 1, img: "../../images/c6.png" },
         { id: 1, img: "../../images/c1.png" },
         { id: 1, img: "../../images/c2.png" },
         { id: 1, img: "../../images/c3.png" },
         { id: 1, img: "../../images/c4.png" },
       ];
       wx.hideNavigationBarLoading();
       newlist = that.data.list.concat(dataList),
       that.setData({
         list: newlist,
       });
     }, 2000);
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})