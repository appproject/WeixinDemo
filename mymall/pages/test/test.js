// pages/test/test.js
const userInfoManager = require("../../common/userInfoManager.js")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    message : 'this is test page',
    array : [1,2,3,4,5],
    view: 'WEBVIEW',
    staffA: { firstName: "zhua", secondName : "ZHENGJUNA"},
    staffB: { firstName: "zhub", secondName: "ZHENGJUNb" },
    staffC: { firstName: "zhuc", secondName: "ZHENGJUNc" },
    count : 1,
    objectArray : [
      { id: 5, unique: "unique_5" },
      { id: 4, unique: "unique_4" },
      { id: 3, unique: "unique_3"},
      { id: 2, unique: "unique_2" },
      { id: 1, unique: "unique_1" },
      { id: 0, unique: "unique_0" }
    ],
    item: { index: 0, msg: "模板消息0", time: "12:10" },
  },

 addCount:function(e){
   this.setData({
     count: this.data.count + 1
   })
 },

 swich:function(e){
   const length = this.data.objectArray.length
   for(let i = 0; i < length; i++){
     const x = Math.floor(Math.random() * length)
     const y = Math.floor(Math.random() * length)
     const temp = this.data.objectArray[x]
     this.data.objectArray[x] = this.data.objectArray[y]
     this.data.objectArray[y] = temp
   }
    this.setData({
        objectArray : this.data.objectArray
    })
 },

  addToFront:function(e){
    const length = this.data.objectArray.length
    this.data.objectArray = [{id : length, unique : "unique_" + length}].concat(this.data.objectArray);
    this.setData({
      objectArray: this.data.objectArray
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    userInfoManager.addUserInfoCallback(this.onUserInfoUpdate)
    if (!userInfoManager.isLogin()) {
      userInfoManager.requestWXLogin()
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    userInfoManager.removeUserInfoCallback(this.onUserInfoUpdate);
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  //定义用户信息更新callback 收到callback之后重新更新UI
  onUserInfoUpdate: function () {
    console.log('test userinfo update')
  }
})