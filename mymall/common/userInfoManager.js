const userInfoCallbcks = []

var wxUserInfo // 微信用户数据
var tokenData // token信息

function init() {
  var token = wx.getStorageSync("token");
  console.log(token.token);
  if (token) { //检查是不是已经登录成功
    wxUserInfo = wx.getStorageSync("wxInfo");
    tokenData = token;
    console.log('username ' + wxUserInfo.nickName);
  } else { //没有登录则跳转去登录
    requestWXLogin()
  }
}

function requestWXLogin() {
  wx.login({
    success: function (res) {
      console.log(res.code)
      if (res.code) {
        wx.getUserInfo({
          withCredentials: true,
          success: function (res_user) {
            // 可以将 res 发送给后台解码出 unionId
            wxUserInfo = res_user.userInfo
            console.log(wxUserInfo);
            requestMyLogin(res_user)
            wx.setStorageSync("wxInfo", wxUserInfo);
          },
          fail: function () {
            wxGetUserInfoFail()
          }
        })
      }
    }
  })
}

///请求自己后台的接口 如果成功了保存用户的token信息 后续操作都需要token触发
function requestMyLogin(res) {
  console.log("requestMyLogin");
  wx.request({
    url: 'https://www.easy-mock.com/mock/5ab22581584a4217a30fc936/test/wxLogin',
    success: function (res_login) {
      console.log(res_login.data.data.token);
      tokenData = res_login.data.data;
      wx.setStorageSync("token", tokenData);
      fireUserInfoUpdate();
    }
  })
}

///获取用户信息没有权限 则提示用户 去设置开启权限 开启权限成功后则继续去请求登录
function wxGetUserInfoFail() {
  console.log("wxGetUserInfoFail");
  wx.showModal({
    title: '警告通知',
    content: '您点击了拒绝授权,将无法正常显示个人信息,点击确定重新获取授权。',
    success: res => {
      if (res.confirm) {
        console.log("ok permission");
        openWxSetting()
      } else {
        console.log("cancel permission");
      }
    }
  })
}

function openWxSetting() {
  wx.openSetting({
    success: res => {
      console.log("openWxSetting success");
      if (res.authSetting["scope.userInfo"]) {////如果用户重新同意了授权登录
        console.log("agress success");
        requestWXLogin()
      } else {
        console.log("no agress success");
      }
    },
    fail: function () {
      console.log("openWxSetting fail");
    }
  })
}

//检查是不是登录成功了
function isLogin() {
  return tokenData != null;
}

//获取用户信息
function getUserInfo(){
  return wxUserInfo;
}

//添加用户信息更新callback
function addUserInfoCallback(callback) {
  for (var i = 0; i < userInfoCallbcks.length; ++i) {
    if (userInfoCallbcks[i] == callback) {
      return
    }
  }
  userInfoCallbcks.push(callback);
  console.log(userInfoCallbcks)
  console.log('addUserInfoCallback ' + userInfoCallbcks.length)
}

//移除用户信息更新callback
function removeUserInfoCallback(callback) {
  for (var i = 0; i < userInfoCallbcks.length; i++) {
    console.log("i " + i)
    if (userInfoCallbcks[i] == callback) {
      console.log("remove " + i)
      userInfoCallbcks.splice(i, 1);
      break
    }
  }
  console.log(userInfoCallbcks)
  console.log('removeUserInfoCallback ' + userInfoCallbcks.length)
}

//触发用户信息更新callback
function fireUserInfoUpdate() {
  console.log('fireUserInfoUpdate ' + userInfoCallbcks.length)
  for (var i = 0; i < userInfoCallbcks.length; ++i) {
    let callback = userInfoCallbcks[i];
    if (typeof callback !== "function") {
      console.log('not callback')
    } else {
      callback()
    }
  }
}

module.exports = {
  addUserInfoCallback: addUserInfoCallback,
  fireUserInfoUpdate: fireUserInfoUpdate,
  removeUserInfoCallback: removeUserInfoCallback,
  init: init,
  isLogin: isLogin,
  getUserInfo: getUserInfo,
}