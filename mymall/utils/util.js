const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

function requestScrollerWindowHeight(callback, fixedHeight) {
  if (typeof callback !== "function") {
    console.log('not callback')
    return
  }
  var height = 0
  wx.getSystemInfo({
    success: function (res) {
      console.log(res);
      //吐槽 习惯了安卓 小程序不能自适应好忧伤 需要确定scroller的height才能滚动
      height = res.windowHeight * 750 / res.windowWidth - fixedHeight;
      height = height.toFixed(0);
      console.log(height);
      callback(height);
    }
  })

  //魅族手机测试在onload时候立即获取高度有点抖动 延迟1s在获取一次 好坑爹
  setTimeout(() => {
    wx.getSystemInfo({
      success: function (res) {
        log(res);
        var newHeight = res.windowHeight * 750 / res.windowWidth - fixedHeight;
        newHeight = newHeight.toFixed(0)
        log("newh=" + newHeight);
        if (height != newHeight) {
          log("not equal")
          callback(newHeight)
        } else {
          log("yes equal")
        }
      }
    })
  }, 1000)
}

function log(msg) {
  if (!msg) return;
  if (getApp().settings['debug'])
    console.log(msg);
  let logs = wx.getStorageSync('logs') || [];
  logs.unshift(msg)
  wx.setStorageSync('logs', logs)
}

module.exports = {
  formatTime: formatTime,
  log: log,
  requestScrollerWindowHeight: requestScrollerWindowHeight,
}
